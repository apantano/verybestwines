var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify-css');

// Sass
gulp.task('sass', function() {
    return gulp.src('sass/gulp-style.sass')
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: 'normal',
            includePaths: require('node-neat').includePaths,
            errLogToConsole: true

        }))
        .pipe(gulp.dest('css'));
});

// Sass Final without comments al line references
gulp.task('sassFinal', function() {
    return gulp.src('sass/gulp-style.sass')
        .pipe(sass({
            outputStyle: 'nested',
            sourceComments: 'false',
            includePaths: require('node-neat').includePaths

        }))
        .pipe(gulp.dest('css/final'));
});

//Minify the final css file
gulp.task('minifyCSS', function(){
   return gulp.src('css/*.css')
       .pipe(minify())
       .pipe(gulp.dest('css/final'))
});


//Parse js files
gulp.task('scripts', function(){
    return gulp.src('js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('js/min'));
});


// Watch Files For Changes
gulp.task('watch', function() {
    livereload.listen();

    gulp.watch(['sass/*.scss', 'sass/*.sass'], ['sass']);
    gulp.watch(['js/*.js'], ['scripts']);

    gulp.watch(['css/**']).on('change', livereload.changed);
    //gulp.watch(['../templates/**']).on('change', livereload.changed);
    gulp.watch(['js/**/*.js']).on('change', livereload.changed);
});

// Default Task
gulp.task('default', ['sass', 'scripts', 'watch']);
// Final css version
gulp.task('minify', ['minifyCSS']);
