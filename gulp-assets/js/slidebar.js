
(function ($) {

    Drupal.behaviors.gulp_slidebar = {
        attach: function (context, settings) {
            function getScrollBarWidth(){
                if($(document).height() > $(window).height()){
                    $('body').append('<div id="fakescrollbar" style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"></div>');
                    fakeScrollBar = $('#fakescrollbar');
                    fakeScrollBar.append('<div style="height:100px;">&nbsp;</div>');
                    var w1 = fakeScrollBar.find('div').innerWidth();
                    fakeScrollBar.css('overflow-y', 'scroll');
                    var w2 = $('#fakescrollbar').find('div').html('html is required to init new width.').innerWidth();
                    fakeScrollBar.remove();
                    return (w1-w2);
                }
                return 0;
            }


            //*************** PUSHY *********************
            var pushy = $('.pushy'), //menu css class
                body = $('body'),
                container = $('#container'), //container css class
                push = $('.push'), //css class to add pushy capability
                siteOverlay = $('.site-overlay'), //site overlay
                pushyClass = "pushy-left pushy-open", //menu position & menu open class
                pushyActiveClass = "pushy-active", //css class to toggle site overlay
                containerClass = "container-push", //container open class
                pushClass = "push-push", //css class to add pushy capability
                menuBtn = $('.menu-btn, .pushy a'), //css classes to toggle the menu
                menuSpeed = 200, //jQuery fallback menu speed
                menuWidth = pushy.width() + "px"; //jQuery fallback menu width

            function togglePushy() {
                body.toggleClass(pushyActiveClass); //toggle site overlay
                pushy.toggleClass(pushyClass);
                container.toggleClass(containerClass);
                push.toggleClass(pushClass); //css class to add pushy capability
            }

            function openPushyFallback() {
                body.addClass(pushyActiveClass);
                pushy.animate({left: "0px"}, menuSpeed);
                container.animate({left: menuWidth}, menuSpeed);
                push.animate({left: menuWidth}, menuSpeed); //css class to add pushy capability
            }

            function closePushyFallback() {
                body.removeClass(pushyActiveClass);
                pushy.animate({left: "-" + menuWidth}, menuSpeed);
                container.animate({left: "0px"}, menuSpeed);
                push.animate({left: "0px"}, menuSpeed); //css class to add pushy capability
            }

            //checks if 3d transforms are supported removing the modernizr dependency
            cssTransforms3d = (function csstransforms3d() {
                var el = document.createElement('p'),
                    supported = false,
                    transforms = {
                        'webkitTransform': '-webkit-transform',
                        'OTransform': '-o-transform',
                        'msTransform': '-ms-transform',
                        'MozTransform': '-moz-transform',
                        'transform': 'transform'
                    };

                // Add it to the body to get the computed style
                document.body.insertBefore(el, null);

                for (var t in transforms) {
                    if (el.style[t] !== undefined) {
                        el.style[t] = 'translate3d(1px,1px,1px)';
                        supported = window.getComputedStyle(el).getPropertyValue(transforms[t]);
                    }
                }

                document.body.removeChild(el);

                return (supported !== undefined && supported.length > 0 && supported !== "none");
            })();

            if (cssTransforms3d) {
                //toggle menu
                menuBtn.click(function () {
                    togglePushy();
                });
                //close menu when clicking site overlay
                siteOverlay.click(function () {
                    togglePushy();
                });
            }
            else {
                //jQuery fallback
                pushy.css({left: "-" + menuWidth}); //hide menu by default
                container.css({"overflow-x": "hidden"}); //fixes IE scrollbar issue

                //keep track of menu state (open/close)
                var state = true;

                //toggle menu
                menuBtn.click(function () {
                    if (state) {
                        openPushyFallback();
                        state = false;
                    } else {
                        closePushyFallback();
                        state = true;
                    }
                });

                //close menu when clicking site overlay
                siteOverlay.click(function () {
                    if (state) {
                        openPushyFallback();
                        state = false;
                    } else {
                        closePushyFallback();
                        state = true;
                    }
                });
            }


            //-----------> PUSHY









            var $sidebar = $('.full-sidebar');
            var $pushyContainer = $('#sidebar-wrapper');
            var $slidyButton = $('#slidy-button');
            var bkp = 1025;
            var status = '';
            var prevStatus = '';


            function getScrollBarWidth(){
                if($(document).height() > $(window).height()){
                    $('body').append('<div id="fakescrollbar" style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"></div>');
                    fakeScrollBar = $('#fakescrollbar');
                    fakeScrollBar.append('<div style="height:100px;">&nbsp;</div>');
                    var w1 = fakeScrollBar.find('div').innerWidth();
                    fakeScrollBar.css('overflow-y', 'scroll');
                    var w2 = $('#fakescrollbar').find('div').html('html is required to init new width.').innerWidth();
                    fakeScrollBar.remove();
                    return (w1-w2);
                }
                return 0;
            }

            var onClickHeader = function(el) {
                $(el.target).toggleClass('open').siblings('ul[class*=facet]').fadeToggle(400);
                $(el.target).closest('section').toggleClass('closed');
            };

            function swapSidebar(loc) {

                if(prevStatus != status) {
                    if (loc == 'mobile') {
                        console.log('changed to Mobile');
                        $('body').removeClass('full-mode').addClass('mobile-mode');
                        $sidebar.addClass('in');
                        $sidebar.appendTo($pushyContainer);
                        attachEvents();
                    }
                    else {
                        console.log('changed to Desk');
                        $('body').removeClass('mobile-mode').addClass('full-mode');
                        $sidebar.prependTo('#container .main-container .row');
                        $sidebar.removeClass('in');
                        detachEvents();
                        //Remove Pushy Classes

                        if(pushy.hasClass('pushy-open'))
                            togglePushy();
                    }
                }
                prevStatus = status;
            }



            function attachEvents() {
                $sidebar.find('section[class*=facet] h2').each(function () {
                    var $this = $(this);
                    var inputStatus = $this.siblings('ul').find('input:checked');
                    $this.closest('section').removeClass('closed');

                    if (inputStatus.length == 0) {
                        $this.siblings('ul').slideToggle();
                        $this.addClass('open');
                        $this.closest('section').addClass('closed');
                    }

                    $this.bind('click', { el: $this }, onClickHeader);
                });
            }

            function detachEvents(){
                $sidebar.find('section[class*=facet] h2').each(function () {
                    var $this = $(this);
                    $this.siblings('ul').slideDown();
                    $this.addClass('open');
                    $this.unbind('click', onClickHeader);

                });
            }


            function Slidify() {
                var size = $(window).width() + getScrollBarWidth();

                if($sidebar.length > 0) {


                    if (size < bkp) { //We are in mobile screens
                        status = 'mobile';
                        swapSidebar('mobile');
                    }
                    else {
                        status = 'desk';
                        swapSidebar('desktop');
                    }
                }
                else{
                    $slidyButton.css('display', 'none');
                }
            }


            $(window).resize(function(){
                //Check the screen size
                Slidify();
            });


            Slidify();

        }
    }

})(jQuery);
